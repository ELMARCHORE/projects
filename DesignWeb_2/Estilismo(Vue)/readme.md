# SALON VALERIA  BERTHY 
                                    UNIVERSIDAD PRIVADA DOMINGO SAVIO

                                    FACULTAD DE CIENCIAS Y TECNOLOGIA

![Alt text](IMG/LOGOOOOOOOOOFIAL.jpeg)

                                          INGENIERIA DE SISTEMAS

                                        SALON VALERIA BERTHY 

                                             INTEGRANTES:

                                        ESTHEFANI PERALTA VILLEGAS 

                                        CESAR EDUARDO  PERALTA  SOTO

                                        EDUARD HEREDIA CHAVEZ 

                                  DOCENTE: ING. JAIME ZAMBRANA CHACÓN

                                            ENERO 2024

                                         Santa cruz – Bolivia

## RESUMEN
Presentamos un pequeño sitio web que muestra nuestra aplicación de conocimientos en Vue, HTML y CSS para un salón de belleza cerca de la zona universitaria. Propiedad de la señora Valerya Berthy con dos trabajadores actuales, nuestro esfuerzo colaborativo resultó en una página visualmente atractiva inspirada en el logo del salón. Este proyecto proporciona información básica sobre servicios, el equipo y detalles de contacto. Esperamos que mejore la presencia en línea del salón, facilitando la interacción con posibles clientes. Agradecidos por el apoyo de la señora Valerya Berthy, anticipamos futuras mejoras. Esta iniciativa refleja nuestra dedicación a la aplicación práctica y colaboración continua en el proyecto en evolución.

## ABSTRACT
Introducing a small website showcasing our application of Vue, HTML and CSS expertise for a beauty salon near the university area. Owned by Mrs. Valerya Berthy with two current workers, our collaborative effort resulted in a visually appealing page inspired by the salon's logo. This project provides basic information about services, equipment and contact details. We hope it will improve the salon's online presence, making it easier to interact with potential clients. Grateful for the support of Ms. Valerya Berthy, we anticipate future improvements. This initiative reflects our dedication to practical application and continued collaboration on the evolving project.
## DESARROLLO

### MARCO TEÓRICO

    JAVASCRIP:
    Es un lenguaje de programación interpretado que se utiliza principalmente para crear aplicaciones web interactivas. Es un componente fundamental de las páginas web modernas y permite la interactividad del usuario, la manipulación del DOM (Modelo de Objetos del Documento) y el desarrollo de aplicaciones web complejas.   

    V-MODEL:

    En programación es un modelo de desarrollo de software que organiza las fases de desarrollo y prueba en forma de "V" para ilustrar la relación entre ellas. Cada fase de desarrollo tiene una fase correspondiente de prueba, y la ejecución de estas fases sigue una secuencia que recuerda la forma de una "V". Este modelo se utiliza principalmente para garantizar una mayor integración entre las actividades de desarrollo y prueba.

    BOOTSTRAP:

    
    Bootstrap es un framework de desarrollo front-end de código abierto que facilita la creación rápida y eficiente de sitios web y aplicaciones web responsivas. Proporciona un conjunto de herramientas y componentes predefinidos basados en HTML, CSS y JavaScript que permiten a los desarrolladores construir interfaces de usuario modernas y atractivas con facilidad. Su sistema de rejilla responsiva, componentes CSS, plugins JavaScript y diseño consistente lo convierten en una opción popular para proyectos de desarrollo web.     

    VUE.JS:

    Es un framework progresivo para construir interfaces de usuario. A diferencia de otros frameworks monolíticos, Vue está diseñado desde cero para ser utilizado incrementalmente. La librería central está enfocada solo en la capa de visualización, y es fácil de utilizar e integrar con otras librerías o proyectos existentes. Por otro lado, Vue también es perfectamente capaz de impulsar sofisticadas Single-Page Applications cuando se utiliza en combinación con herramientas modernas y librerías de apoyo.  

    HTML:

    HTML, que significa "HyperText Markup Language" (Lenguaje de Marcado de Hipertexto), es el lenguaje estándar utilizado para crear y diseñar páginas web. Fue creado por Tim Berners-Lee en la década de 1990 como parte del desarrollo del World Wide Web (WWW) en el CERN (Organización Europea para la Investigación Nuclear).

    HTML proporciona la estructura básica y los elementos necesarios para crear páginas web. Es un lenguaje de marcado que utiliza etiquetas (tags) para definir la estructura y el contenido de una página web. Las etiquetas HTML se utilizan para marcar diferentes tipos de contenido, como texto, imágenes, enlaces, formularios, videos, entre otros.

    Cada elemento en HTML está encapsulado dentro de etiquetas, que tienen una sintaxis específica y describen la función y el contenido del elemento.  
    
    V-FOR:
    
    Es una característica en Vue.js que te permite repetir o mostrar elementos en tu página web según los datos que tengas en una lista. Por ejemplo, puedes usar v-for para mostrar una lista de nombres, imágenes, o cualquier otro contenido que tengas en un conjunto de datos. Es una manera de automatizar la creación de elementos HTML basándote en la información que tengas en tu aplicación


### PROYECTO

#### Diseño
    Nuestra pagina web  de  la  empresa  "Valeria  Berthy" es un acogedor salon en la que  se especializa  en la belleza y cuidado  personal 
    teniendo un equipo talentoso  de  estilistas y  profesionales la cual esta dedicado a resaltar  tu belleza natural.
    la pagina  web  en la que estamos trabajando tiene  la siguiente  caracteristicas  cuanta  con  cuatros  pestañas las  cuales son   inicio, catalogo, reservas  e   informacion.  
    
![Alt text](IMG/logofinal.PNG)

#### Funcionalidades

                                                 CATALOGO 

    La  pagina  de catalogo cuenta  con tres imagenes  principales  que  son  estilismo, mirada  radiante y uñas. 
![Alt text](IMG/catologo1.PNG)
                                
                                                 ESTILISMO 

    En la parte de estilismo podemos ver  la variedad de peinados, alisado y tramientos para  el cabello.

![Alt text](IMG/estilismo.PNG)

                                                MIRADA RADIANTE

   En mirada  radiente  encontramos la variedad de  maquillaje para todo tipo de  eventos, asi como  depilado de  cejas  y  baño de  luna.

![Alt text](IMG/radiante.PNG)

                                                     UÑAS 

   Aqui encontraremos diferentes tipos de trabajo tanto  en manicure y  pedicure.

![Alt text](IMG/uñas.PNG)

                                                    RESERVAS

    En la parte de  reserva  el cliente  pondra su nombre y  podra  elegir  el  empleador que lo va atender , asi  como  tambien  el trabajo que  quiere realizarse  y la  fecha. 

![Alt text](IMG/RESERVAS.PNG)


## CONCLUSIÓN

    En la culminación de este proyecto, hemos llevado a cabo una minuciosa implementación de los últimos detalles, alcanzando con éxito la fase final. El marco teórico ha sido enriquecido mediante la integración de detalladas descripciones de las herramientas utilizadas, proporcionando una comprensión más profunda de nuestra labor. La interacción fluida con VUE.JS, BotsTrap, HTML y CSS ha dado forma a la página resultante, incorporando con destreza funcionalidades únicas de cada programa.

    Adicionalmente, hemos incorporado la ubicación del emprendimiento seleccionado, agregando un componente relevante que contextualiza nuestra iniciativa en términos geográficos y empresariales. Esta conclusión no solo señala el cierre del proyecto, sino también la dedicación meticulosa a cada aspecto, desde la teoría hasta la implementación práctica.

## BIBLIOGRAFÍA

    Chat GPT. (2020). Chat Openai. Obtenido de https://chat.openai.com/

    Edgar Rangel  (11 de junio ). YouTube. Obtenido de https://youtu.be/mev6MBy4SQ8?si=LZARl-h3g9TG_I-z

    Bootstrap  (12 de octubre de 2021). Google  Obtenido de https://www.hostinger.mx/tutoriales/que-es-bootstrap

    VUE.JS   Google obtenido en  https://es.vuejs.org/v2/guide/