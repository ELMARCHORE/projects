Vue.createApp({
    data() {
        return {
            mostrarMatematicas: false,
            mostrarBiologia: false,
            mostrarHistoria: false,
            mostrarLiteratura: false,
            cantidadCarrito: 0,
            mostrarCarrito:false,
            cantidadExcedida:'Ingrese una cantidad inferior a la cantidad de libros',
            cantidadInferior:'Ingrese una cantidad superior a 0',
            libro: [
                { id:"1",nombre: "El Codigo Da Vinci", materia: "solicitado", autor: "Dan Brown", foto: "img/El-codigo-da-vinci.webp", reseña:"Una intrigante novela de misterio y suspense que sigue al profesor Robert Langdon en su búsqueda de revelaciones ocultas en obras de arte y símbolos, desentrañando secretos que podrían cambiar la historia tal como la conocemos.", precio: 30,cantidad:1,stock:90 },
                { id:"2",nombre: "El Señor de los Anillos", materia: "solicitado", autor: "J. R. R. Tolkien", foto: "img/El-Senor-de-los-Anillos-Ilustrado-por-Alan-Lee.webp", reseña:"Una épica fantasía que sigue la odisea de Frodo Bolsón para destruir un anillo maligno y salvar a la Tierra Media. Llena de personajes inolvidables y un mundo ricamente detallado, es una obra maestra del género.", precio: 30,cantidad:1,stock:80 },
                { id:"3",nombre: "El Principito", materia: "solicitado", autor: "Antoine de Saint-Exupéry", foto: "img/El-Principito.webp", reseña:"Una conmovedora historia que explora temas profundos a través de las conversaciones entre un aviador accidentado y un pequeño príncipe proveniente de otro planeta. Llena de sabiduría y reflexiones sobre la vida.", precio: 30,cantidad:1,stock:30 },
                { id:"4",nombre: "Don Quijote de la Mancha", materia: "solicitado", autor: "Miguel de Cervantes", foto: "img/Don-Quijote-de-la-Mancha.webp", reseña:"Considerada una de las obras más importantes de la literatura mundial, esta novela cómica sigue las aventuras del caballero loco Don Quijote y su fiel escudero Sancho Panza. Una sátira ingeniosa de la sociedad y las novelas de caballería.", precio: 30,cantidad:1,stock:85 },
                { id:"5",nombre: "El diario de Ana Frank", materia: "recomendado", autor: "Ana Frank", foto: "img/El-Diario-De-Ana-Frank.webp", reseña:"Un testimonio conmovedor de la vida de Ana Frank, una niña judía que se escondió con su familia durante la Segunda Guerra Mundial. Su diario captura la lucha por la supervivencia y sus reflexiones sobre la humanidad en tiempos difíciles.", precio: 30,cantidad:1,stock:95 },
                { id:"6",nombre:"El Alquimista", materia: "recomendado", autor:"Paulo Coelho", foto:"img/El-alquimista.webp", reseña:"Una cautivadora novela que sigue el viaje espiritual de Santiago, un joven pastor, en busca de su tesoro personal. A través de encuentros con personajes místicos, la historia explora la realización de los sueños y la conexión con el universo.", precio: 30,cantidad:1,stock:40},
                { id:"7",nombre:"Harry Potrer", materia: "recomendado", autor:"J. K. Rowling", foto:"img/Harry-Potter-La-Coleccion-Completa.webp", reseña:"Una serie mágica que sigue las aventuras de Harry Potter, un joven mago, en su lucha contra el malvado Lord Voldemort. Llena de magia, amistad y valentía, la saga ha cautivado a lectores de todas las edades en todo el mundo.", precio: 30,cantidad:1,stock:20},
                { id:"8",nombre:"Cien Años de Soledad", materia: "infantile", autor:"Gabriel García Márquez", foto:"img/Cien-anos-de-soledad.webp", reseña:"Una obra maestra del realismo mágico que narra la historia de la familia Buendía a lo largo de varias generaciones en el pueblo ficticio de Macondo. La novela explora temas como el amor, la soledad y el destino.", precio: 30,cantidad:1,stock:70},
                { id:"9",nombre:"El nombre de la Rosa", materia: "infantile", autor:"Umberto Eco", foto:"img/El-nombre-de-la-rosa.webp", reseña:"Un thriller histórico que sigue al monje franciscano Guillermo de Baskerville y su aprendiz Adso mientras investigan misteriosos asesinatos en una abadía benedictina en el siglo XIV. La trama combina intriga, historia y reflexiones filosóficas.", precio: 30,cantidad:1,stock:65},
                { id:"10",nombre:"Historia de dos Ciudades", materia: "infantile", autor:"Charles Dickens", foto:"img/Historia-de-dos-ciudades.webp", reseña:"Ubicada en la tumultuosa época de la Revolución Francesa, esta novela narra la historia de varios personajes entrelazados en Londres y París. Conmovedora y llena de drama, explora temas de sacrificio y redención.", precio: 30,cantidad:1,stock:72},
                { id:"11",nombre: "Álgebra Lineal", materia: "matematica", autor: "Howard Anton", foto: "img/m11.jpg", reseña: "Un libro introductorio que aborda conceptos fundamentales de álgebra lineal y sus aplicaciones.", precio: 30,cantidad:1,stock:55 },
                { id:"12",nombre: "Cálculo: Una Variable", materia: "matematica", autor: "James Stewart", foto: "img/m12.jpg", reseña: "Un libro clásico que cubre los principios del cálculo diferencial e integral.", precio: 33,cantidad:1,stock:150},
                { id:"13",nombre: "Geometría Analítica", materia: "matematica", autor: "Charles H. Lehmann", foto: "img/m13.jpg", reseña: "Un enfoque detallado en la geometría analítica, explorando las relaciones entre álgebra y geometría.", precio: 40,cantidad:1,stock:110 },
                { id:"14",nombre: "Teoría de Números", materia: "matematica", autor: "Ivan Niven", foto: "img/m14.jpg", reseña: "Un libro que explora propiedades y teoremas relacionados con los números enteros y sus propiedades.", precio: 18,cantidad:1,stock:40},
                { id:"15",nombre: "Probabilidad y Estadística", materia: "matematica", autor: "Morris H. DeGroot", foto: "img/m15.jpg", reseña: "Un texto que introduce conceptos clave en probabilidad y estadística, con aplicaciones prácticas.", precio: 25,cantidad:1,stock:70 },
                { id:"16",nombre: "Topología General", materia: "matematica", autor: "John L. Kelley", foto: "img/m16.jpg", reseña: "Esta obra es una referencia clásica en el estudio de la topología. Kelley presenta los conceptos de manera rigurosa y formal, cubriendo temas como conjuntos abiertos y cerrados, continuidad, compacidad y conexidad.", precio: 20,cantidad:1,stock:80 },
                { id:"17",nombre: "Introducción a la Teoría de Grupos", materia: "matematica", autor: "Joseph J. Rotman", foto: "img/m17.jpg", reseña: "Rotman ofrece una introducción accesible a la teoría de grupos. El libro cubre conceptos fundamentales como subgrupos, homomorfismos, grupos cíclicos y teoremas de isomorfismo, con aplicaciones en álgebra y geometría.", precio: 35,cantidad:1,stock:90 },
                { id:"18",nombre: "El último teorema de Fermat", materia: "matematica", autor: "Simon Singh", foto: "img/m18.webp", reseña: "Singh narra la historia del famoso teorema de Fermat, desde su enunciado hasta su demostración, explorando la vida de los matemáticos que intentaron resolver este enigma durante siglos.", precio: 27,cantidad:1,stock:50 },
                { id:"19",nombre: "La música de los números primos", materia: "matematica", autor: "Marcus du Sautoy", foto: "img/m19.jpg", reseña: "Du Sautoy explora la relación entre los números primos y la música, mostrando cómo los patrones numéricos se pueden encontrar en la estructura de la música y viceversa.", precio: 15,cantidad:1,stock:130 },
                { id:"20",nombre: "El hombre que calculaba", materia: "matematica", autor: "Malba Tahan", foto: "img/m20.jpg", reseña: "Una novela que combina la narrativa con problemas matemáticos, siguiendo las aventuras de un hábil calculador que resuelve problemas intrigantes con su astucia y conocimientos matemáticos.", precio: 50,cantidad:1,stock:40 },
                { id:"21",nombre: "Biología Celular y Molecular", materia: "biologia", autor: "Bruce Alberts", foto: "img/b16.jpg", reseña: "Un libro que explora los principios fundamentales de la biología celular y molecular.", precio: 30,cantidad:1,stock:80},
                { id:"22",nombre: "Evolución", materia: "biologia", autor: "Douglas J. Futuyma", foto: "img/b17.jpg", reseña: "Un texto que aborda los conceptos clave de la teoría de la evolución y sus aplicaciones en la biología.", precio: 30,cantidad:1,stock:70},
                { id:"23",nombre: "Ecología", materia: "biologia", autor: "Eugene P. Odum", foto: "img/b18.jpg", reseña: "Un libro que examina las interacciones entre los organismos y su entorno, explorando los principios de la ecología.", precio: 30,cantidad:1,stock:46},
                { id:"24",nombre: "Genética", materia: "biologia", autor: "Peter J. Russell", foto: "img/b19.jpg", reseña: "Un enfoque completo en los principios de la genética, incluyendo la herencia y la manipulación genética.", precio: 30,cantidad:1,stock:81},
                { id:"25",nombre: "El origen de las especies", materia: "biologia", autor: "Charles Darwin", foto: "img/b22.jpg", reseña: "Considerado un clásico de la biología, este libro presenta la teoría de la evolución por selección natural, revolucionando nuestra comprensión de la diversidad de la vida en la Tierra.", precio: 30,cantidad:1,stock:24},
                { id:"26",nombre: "Biología Marina", materia: "biologia", autor: "Peter Castro", foto: "img/b20.jpg", reseña: "Un libro que introduce los aspectos biológicos de la vida marina, desde microorganismos hasta ecosistemas acuáticos.", precio: 30,cantidad:1,stock:100},
                { id:"27",nombre: "El gen egoísta", materia: "biologia", autor: "Richard Dawkins:", foto: "img/b23.jpg", reseña: "Dawkins explora la teoría de la evolución desde la perspectiva de los genes, argumentando que son los verdaderos impulsores de la evolución y explicando cómo influyen en el comportamiento animal y humano.", precio: 30,cantidad:1,stock:26},
                { id:"28",nombre: "La doble hélice", materia: "biologia", autor: "James D. Watson", foto: "img/b24.jpg", reseña: "En este libro, Watson relata la emocionante historia del descubrimiento de la estructura del ADN, ofreciendo una visión fascinante de los avances científicos y las rivalidades en el campo de la genética.", precio: 30,cantidad:1,stock:78},
                { id:"29",nombre: "Silent Spring", materia: "biologia", autor: "Rachel Carson", foto: "img/b25.png", reseña: "Este libro, considerado un hito en la historia de la conservación ambiental, alerta sobre los peligros de los pesticidas y otros productos químicos en el medio ambiente, inspirando el movimiento ambientalista moderno.", precio: 30,cantidad:1,stock:51},
                { id:"30",nombre: "La evolución de la cooperación", materia: "biologia", autor: "Robert Axelrod", foto: "img/b27.jpg", reseña: "Axelrod examina cómo la cooperación ha evolucionado en diferentes contextos biológicos y sociales, ofreciendo ideas valiosas sobre la naturaleza y los beneficios de la cooperación en la vida humana y animal.", precio: 30,cantidad:1,stock:140},
                { id:"31",nombre: "Sapiens: De Animales a Dioses", materia: "historia", autor: "Yuval Noah Harari", foto: "img/h21.jpg", reseña: "Un libro que explora la historia de la humanidad desde los primeros homínidos hasta la era actual, abordando grandes cambios culturales y sociales.", precio: 30,cantidad:1,stock:47},
                { id:"32",nombre: "Breve Historia del Tiempo", materia: "historia", autor: "Stephen Hawking", foto: "img/h22.jpg", reseña: "Una obra que aborda conceptos fundamentales de la física y la cosmología, contextualizando la historia del universo.", precio: 30,cantidad:1,stock:35},
                { id:"33",nombre: "El Arte de la Guerra", materia: "historia", autor: "Sun Tzu", foto: "img/h23.jpg", reseña: "Un antiguo tratado militar que aborda estrategias y tácticas en el arte de la guerra, con aplicaciones históricas y contemporáneas.", precio: 30,cantidad:1,stock:85},
                { id:"34",nombre: "Historia del Mundo en 100 Objetos", materia: "historia", autor: "Neil MacGregor", foto: "img/h24.jpg", reseña: "Un enfoque único que narra la historia del mundo a través de la interpretación de 100 objetos clave.", precio: 30,cantidad:1,stock:80},
                { id:"35",nombre: "Colapso: Por Qué las Sociedades Fallan", materia: "historia", autor: "Jared Diamond", foto: "img/h25.jpg", reseña: "Un análisis que examina las razones detrás del colapso de sociedades a lo largo de la historia, explorando lecciones para el presente.", precio: 30,cantidad:1,stock:100},
                { id:"36",nombre: "El imperio español: De Colón a Magallanes", materia: "historia", autor: "Hugh Thomas", foto: "img/h11.jpg", reseña: "Thomas ofrece una historia detallada del imperio español, desde los viajes de Colón hasta la expedición de Magallanes, explorando el impacto del imperio en América y en el mundo.", precio: 30,cantidad:1,stock:60},
                { id:"37",nombre: "Los mitos de la historia argentina", materia: "historia", autor: "Felipe Pigna", foto: "img/h12.png", reseña: "Pigna desafía las interpretaciones convencionales de la historia argentina, analizando los mitos y las verdades detrás de los eventos y las figuras clave del pasado del país.", precio: 30,cantidad:1,stock:25},
                { id:"38",nombre: "La guerra civil española", materia: "historia", autor: "Antony Beevor", foto: "img/h13.jpg", reseña: "Beevor ofrece una historia completa y detallada de la guerra civil española, explorando los conflictos políticos y sociales que llevaron al estallido del conflicto y sus consecuencias duraderas.", precio: 30,cantidad:1,stock:35},
                { id:"39",nombre: "Los Hermanos Wright ", materia: "historia", autor: "David McCullough", foto: "img/h14.jpg", reseña: "McCullough narra la historia de los hermanos Wright y su innovación pionera en la aviación, ofreciendo una visión detallada de su determinación y logros en el campo del vuelo.", precio: 30,cantidad:1,stock:58},
                { id:"40",nombre: "La historia del mundo en 100 objetos", materia: "historia", autor: "Neil MacGregor:", foto: "img/h15.jpg", reseña: "MacGregor ofrece una mirada única a la historia mundial a través de una selección de 100 objetos, cada uno de los cuales representa un momento significativo en la historia de la humanidad.", precio: 30,cantidad:1,stock:80},
                { id:"41",nombre: "Cien años de soledad", materia: "literatura", autor: "Gabriel García Márquez", foto: "img/l26.jpg", reseña: "Una obra maestra del realismo mágico que narra la historia de la familia Buendía en el pueblo ficticio de Macondo.", precio: 30,cantidad:1,stock:110},
                { id:"42",nombre: "1984", materia: "literatura", autor: "George Orwell", foto: "img/l27.jpg", reseña: "Una novela distópica que presenta un futuro totalitario, explorando temas de control gubernamental y manipulación de la información.", precio: 30,cantidad:1,stock:70},
                { id:"43",nombre: "Orgullo y prejuicio", materia: "literatura", autor: "Jane Austen", foto: "img/l28.jpg", reseña: "Una novela clásica que examina las relaciones sociales y románticas en la Inglaterra del siglo XIX, con un toque de ironía y humor.", precio: 30,cantidad:1,stock:36},
                { id:"44",nombre: "Don Quijote de la Mancha", materia: "literatura", autor: "Miguel de Cervantes", foto: "img/l29.jpg", reseña: "Considerada una de las obras más importantes de la literatura mundial, esta novela cómica sigue las aventuras del caballero loco Don Quijote.", precio: 30,cantidad:1,stock:56},
                { id:"45",nombre: "Crimen y castigo", materia: "literatura", autor: "Fiodor Dostoievski", foto: "img/l30.jpg", reseña: "Una novela psicológica que explora las consecuencias morales y psicológicas de un asesinato cometido por un estudiante en la Rusia del siglo XIX.", precio: 30,cantidad:1,stock:90},
                { id:"46",nombre: "Moby Dick", materia: "literatura", autor: "Herman Melville", foto: "img/l11.jpg", reseña: "Esta épica novela narra la obsesión del capitán Ahab por vengarse de la ballena blanca, Moby Dick, en una travesía llena de simbolismo y metáfora.", precio: 30,cantidad:1,stock:40},
                { id:"47",nombre: "Rayuela", materia: "literatura", autor: "Julio Cortázar", foto: "img/l12.jpg", reseña: "Esta novela experimental sigue las aventuras del intelectual argentino Horacio Oliveira en París y Buenos Aires, desafiando las convenciones narrativas tradicionales.", precio: 30,cantidad:1,stock:50},
                { id:"48",nombre: "Romeo y Julieta", materia: "literatura", autor: "William Shakespeare", foto: "img/l13.jpg", reseña: "Esta tragedia romántica cuenta la historia de amor prohibido entre Romeo Montesco y Julieta Capuleto, que termina en tragedia debido al conflicto entre sus familias.", precio: 30,cantidad:1,stock:110},
                { id:"49",nombre: "La Odisea", materia: "literatura", autor: "Homero", foto: "img/l14.jpg", reseña: "Esta epopeya clásica sigue las aventuras de Odiseo en su viaje de regreso a casa después de la Guerra de Troya, enfrentando monstruos, dioses y obstáculos en su camino.", precio: 30,cantidad:1,stock:20},
                { id:"50",nombre: "Los miserables", materia: "literatura", autor: "Victor Hugo:", foto: "img/l15.jpg", reseña: "Esta novela épica sigue las vidas entrelazadas de varios personajes en la Francia del siglo XIX, explorando temas de redención, justicia social y amor.", precio: 30,cantidad:1,stock:150},
            ],
            libros:[],
        };
    },    
    methods:{
        Carrito(producto){
            if ((producto.cantidad > 0)&&(producto.cantidad<=producto.stock)) {
                this.libros.push(producto);
                this.cantidadCarrito++;
            }else{
                if (producto.cantidad <=0) {
                    this.cantidadInferior;
                }else{
                    this.cantidadExcedida;
                }
            }
        },
        Comprar(producto){
            if ((producto.cantidad > 0)&&(producto.cantidad<=producto.stock)) {
                alert("su compra se realizo con exito")
            }else{
                if (producto.cantidad <=0) {
                    alert("no se puede realizar la compra introduzca un numero valido")
                }else{
                    alert("no se puede realizar la compra ya que la cantidad de libros seleccionada supera el limite de libros")
                }
            }
        },
        openCarr(){
            this.mostrarCarrito=true;
        },
        closeCarr(){
            this.mostrarCarrito=false;
        },
    }
}).mount('#app');