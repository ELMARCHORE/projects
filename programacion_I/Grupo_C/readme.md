## JUEGO DEL LABERINTO
### INTRODUCCION:
El laberinto es un espacio para ser recorrido y se encuentra en el límite entre el paisaje y la arquitectura. El espacio laberíntico se caracteriza porque permite experimentar una cierta iniciación o prueba, a través del recorrido de sus diferentes rutas, mientras se busca el centro o la salida del mismo.

El juego que realizamos en grupo fue un laberinto que consta con dos jugadores que compiten en llegar a un punto verde que se encuentra en medio del laberinto, trata de agarrar la bolita verde en un laberinto donde si el cuadrado rojo chocha con el cuadrado azul el rojo regresa a su punto de inico y si el azul choca con el rojo le pasa lo mismo y si ambos lo hacen regresara a su punto de inico uno de los 2 eso es aleatorio.

### JUGABILIDAD:
Para poder mover los cuadrado (jugadores), estan configurado para ser movido por las teclas del teclado "up", "down", "rigth", "left", estas teclas hacen que el cuadrado del jugador se mueva hacias la izquierda, derecha, arriba y abajo.

Tambien el laberinto esta codificado para que haya un solo camino hacia el obejetivo, tanto para el jugador 1 que el jugador 2.

### INSTALACION DE LIBRERIA:
Las librerias que usamos fue "pygame" y "sys"

## PYGAME:
Pygame es una librería para el desarrollo de videojuegos en segunda dimensión 2D con el lenguaje de programación Python. Pygame está basada en SDL, que es una librería que nos provee acceso de bajo nivel al audio, teclado, ratón y al hardware gráfico de nuestro ordenador. Es un producto que funciona en cualquier sistema: Mac OS, Windows o Linux. El SDL son bibliotecas en lenguaje C para gestión de gráficos 2D (manipulación de las imágenes como objetos de 2D en el lienzo, es decir, la ventana), imágenes (ficheros de tipo jpg o png o tif), audio y periféricos a bajo nivel (teclado, ratón).
 
 ### Estructura de un juego con Pygame:
Tras saber qué es Pygame y como instalar pygame en visual studio code, es importante hablar de su estructura. La estructura básica de un videojuego con programación entre Pygame y Python orientada a objetos se define por:

Una preparación del entorno: primero, debemos saber como importar pygame en python, en nuestro entorno virtual, pues es una librería que no forma parte del startup de Python. Esto lo hacemos con la instrucción: pip install pygame.
Bucle principal de evento-actualización-repintado: nace del contenedor de nuestro videojuego. Allí encontramos el constructor y la función del lanzamiento del videojuego. Este último creará el bucle con funciones como start, mainloop y handleEvent.
Finalización del juego: cuando se finaliza Pygame, es decir, el juego; GameOver.

## SYS:
SYS es una libreria estandar y sirve para manejar parametros y funciones especificas. Este módulo provee acceso a algunas variables usadas o mantenidas por el intérprete y a funciones que interactúan fuertemente con el intérprete. Siempre está disponible.

### RESUMEN:
En resumen, el juego del laberinto es una clasico en los demos, es muy divertido y entretenido ya que hay que saber la ruta ideal para llegar al objetivo.









