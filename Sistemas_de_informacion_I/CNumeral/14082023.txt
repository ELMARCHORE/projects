Readme del Proyecto:

Como grupo, creen un archivo "README.md" que describa brevemente su sistema de información.

Fundamentación del Sistema:
Redacten un documento que explique la razón de ser de su sistema, el problema que resuelve 
y su 

importancia.

Trabajo Colaborativo:
Muestren evidencias de su trabajo en equipo, como chats, reuniones o correos.

Distribución de Roles:
Detallen las responsabilidades y aportes de cada miembro en el proyecto.