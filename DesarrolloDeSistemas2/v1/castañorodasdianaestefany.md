# PROYECTO FORMATIVO MODELADO DE BACKEND DE UN SISTEMA DE GESTION DE LIBRERIA .
- Diana Estefany Castaño Rodas:
- [Correo Electronico](https://mail.google.com/mail/u/0/#inbox)
- [Facebook](https://www.facebook.com/profile.php?id=100009750429193)


## Introduccion
 Este proyecto esta diseñado para el backend de un sistema de gestion de ventas de una libreria, 
 presenta su Sistema de Gestión, una plataforma integral diseñada para mejorar y facilitar el proceso de administración y venta de una amplia gama de productos, desde materiales de escritorio hasta libros educativos. Con una interfaz intuitiva y funciones sólidas, nuestro sistema busca proporcionar una experiencia excepcional tanto para el personal de la librería como para los clientes.

## Objetivos 
*Comprender Requisitos del Cliente : 
Analizar y comprender los requisitos y expectativas del cliente para la implementación del sistema de gestión de librería.

*Diseño del Modelo de Datos:
Elaborar un modelo de datos eficiente que refleje la estructura de la librería, considerando entidades como sucursales, empleados, productos, ventas, editoriales y libros.

*Desarrollo del Backend:
Implementar las funciones y servicios necesarios en el backend para interactuar con la base de datos, gestionando operaciones como registro de sucursales, empleados, productos, ventas, etc.

*Implementación de la Base de Datos:
Crear y configurar la base de datos según el modelo de datos diseñado, garantizando la normalización y las relaciones adecuadas entre las tablas.

## Marco Teorico
## Laravel 
Laravel es un framework de desarrollo web de código abierto basado en el lenguaje de programación PHP. Fue creado por Taylor Otwell y lanzado inicialmente en 2011. Laravel se ha vuelto muy popular en la comunidad de desarrollo web debido a su elegante sintaxis, su enfoque en la legibilidad del código y su capacidad para simplificar tareas comunes.

<a data-flickr-embed="true" href="https://www.flickr.com/photos/200265160@N08/53565706728/in/dateposted-public/" title="laravel8530"><img src="https://live.staticflickr.com/65535/53565706728_38d2a4dca7.jpg" width="500" height="375" alt="laravel8530"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

[documentacion de Laravel](https://laravel.com/docs/10.x)

## Laragon
Laragon es un entorno de desarrollo Web que mediante un único instalador nos instala PHP, Apache, MySQL y Node.js, ahorrándonos una gran cantidad de tiempo al no tener que instalar y configurar todos estos paquetes por separado.
Algunas cualidades de este entorno de desarrollo:

-Múltiples versiones de PHP: Siempre me encontraba el problema de que algún cliente tenía scripts que requerían extensiones especiales o versiones de PHP en concreto. Con Laragon se acabo el problema, puedes tener todas las versiones de PHP que quieras y con diferentes configuraciones y cambiarlas a tu gusto.

-Rápido y Ligero: El núcleo de Laragon consume 4MB de memoria RAM y además puedes detenerlo y ejecutarlo cuando quieras.

-URLs amigables: Si has trabajado con otros entornos sabrás que normalmente todos los proyectos cuelgan de localhost y cuando tienes una cantidad alta de proyectos se convierte en un lío importante. Laragon aparte de poder utilizar localhost te genera un dominio ficticio para cada proyecto, por ejemplo, el proyecto «CRM» en lugar de http://localhost/crm tendríamos http://crm.test.

-Instalación con un click: Seguramente más de una vez has tenido que instalar algún CMS o framework y sabrás lo tedioso que es esto. Con Laragon puedes hacerlo con un clic y el se encargará de realizar todos los pasos.

-Portable: Si utilizas varios ordenadores sabrás lo difícil que es transportar un entorno de desarrollo web, con Laragon dispones de una versión portable que facilita su transporte.

-Habilitar SSL fácilmente: Hoy en día no puedes tener un sitio Web sin SSL, si desarrollas tu sitio Web en http en local y luego lo publicas en un hosting con https puedes encontrarte problemas, mejor desarrollarlo utilizando SSL en local también.

<a data-flickr-embed="true" href="https://www.flickr.com/photos/200265160@N08/53565525746/in/dateposted-public/" title="laragon"><img src="https://live.staticflickr.com/65535/53565525746_49233262ac_o.jpg" width="356" height="142" alt="laragon"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

[Descarga Laragon aqui](https://laragon.org/download/)


## HeidiSql
Es un software que nos permite administrar bases de datos de diversos tipos por medio de interfaz gráfica de usuario (GUI). Es un programa gratuito y de código abierto que se puede usar de manera libre en cualquier situación, personal, profesional, comercial, etc.
Sus características y funcionalidades son las típicas que encontramos en las herramientas de administración de las bases de datos con interfaz gráfica, aunque destaca por ser un software muy maduro y consolidado. 
Casos de uso de HeidiSQL :
Dentro del marco del desarrollo y administración de las bases de datos, podemos destacar los siguientes casos de uso de HeidiSQL.

- Administración de bases de datos MySQL con HeidiSQL:
Es posible realizar la administración de bases de datos utilizando diferentes sistemas gestores en el mismo programa. También ofrece una amplia gama de herramientas y funcionalidades para crear y mantener las bases de datos.

- Gestión de tablas y datos de manera eficiente:
Una de las tareas habituales consiste en mantener los modelos de datos, permitiendo crear, alterar y eliminar tablas de manera sencilla y rápida. Asimismo, es posible mantener los datos que encontramos en ellas, es decir, insertar, actualizar y eliminar registros.

- Optimización de consultas SQL en HeidiSQL:
Otro de los marcos donde podemos sacarle partido es en la optimización de las consultas gracias a su potente editor SQL, que nos ofrece ayudas en el momento de escribir las consultas y sugerencias inteligentes.

<a data-flickr-embed="true" href="https://www.flickr.com/photos/200265160@N08/53565983495/in/dateposted-public/" title="heidilogo"><img src="https://live.staticflickr.com/65535/53565983495_2f00ae6b11_o.png" width="960" height="402" alt="heidilogo"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## StartUML
StarUML genera todo tipo de diagramas compatibles con la plataforma de programas Microsoft Office.StarUML se maneja con facilidad. En un vistazo a la interfaz se ven las funciones principales del programa. Otra característica importante del programa es que su código es compatible con C++ y Java.Puedes comenzar a dibujar los gráficos manualmente o seleccionar las plantillas que contiene el archivo de instalación para modificarlas. 

<a data-flickr-embed="true" href="https://www.flickr.com/photos/200265160@N08/53568148993/in/dateposted-public/" title="UMLstart"><img src="https://live.staticflickr.com/65535/53568148993_e5f7be56b1_o.jpg" width="1280" height="720" alt="UMLstart"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

[Descarga StartUML aqui](https://staruml.softonic.com/)
##	Modelado o Sistematización
### Diagrama de Clases
<a data-flickr-embed="true" href="https://www.flickr.com/photos/200265160@N08/53565879945/in/dateposted-public/" title="modeladoLibreria"><img src="https://live.staticflickr.com/65535/53565879945_36f02bddce_h.jpg" width="1352" height="936" alt="modeladoLibreria"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

### Diagrama entidad relacion
<a data-flickr-embed="true" href="https://www.flickr.com/photos/200265160@N08/53568390005/in/dateposted-public/" title="DiagramaER"><img src="https://live.staticflickr.com/65535/53568390005_13a65d9f12_o.png" width="1213" height="792" alt="DiagramaER"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


### Migracion
```bash
<?php
class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->id();
            $table->string('nombreCliente')->nullable();
            $table->string('apellidoPaterno')->nullable();
            $table->string('apellidoMaterno')->nullable();
            $table->string('telefono')->nullable();
            $table->string('direccionCliente')->nullable();
            $table->string('email');
    
            // Eliminado el campo 'pedido_id' y la relación con la tabla 'pedido'
    
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('categoria', function (Blueprint $table) {
            $table->id();
            $table->string('nombreCategoria')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pedido', function (Blueprint $table) {
            $table->id();
            $table->date('fechaPedido')->nullable();
            $table->string('EstadoPedido')->nullable();
    
            $table->unsignedBigInteger('cliente_id');
            $table->foreign('cliente_id')->references('id')->on('cliente');
    
            // Eliminado el campo 'producto_id' y la relación con la tabla 'producto'
    
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('producto', function (Blueprint $table) {
            $table->id();
            $table->string('NombreProducto')->nullable();
            $table->string('Unidad')->nullable();
            $table->string('descripcion')->nullable();
    
            // Eliminado el campo 'pedido_id' y la relación con la tabla 'pedido'
    
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categoria');
    
            $table->timestamps();
            $table->softDeletes();
        });

       

        Schema::create('detalle_pedido', function (Blueprint $table) {
            $table->id();
            $table->string('DescripcionPedido');
    
            $table->unsignedBigInteger('pedido_id');
            $table->foreign('pedido_id')->references('id')->on('pedido');
    
            $table->unsignedBigInteger('producto_id');
            $table->foreign('producto_id')->references('id')->on('producto');
    
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('libreria', function (Blueprint $table) {
            $table->id();
            $table->string('nombreLibreria');
            $table->string('direccionLibreria');
    
            $table->unsignedBigInteger('cliente_id');
            $table->foreign('cliente_id')->references('id')->on('cliente');
    
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('empleado', function (Blueprint $table) {
            $table->id();
            $table->string('Cargo');
            $table->string('Horario');
    
            $table->unsignedBigInteger('pedido_id');
            $table->foreign('pedido_id')->references('id')->on('pedido');
    
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('zona', function (Blueprint $table) {
            $table->id();
            $table->string('nombreZona')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ciudad', function (Blueprint $table) {
            $table->id();
            $table->string('nombreCiudad')->nullable();
            $table->string('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
```
## explicacion del caso de modelado

### Datos seeder

                                             
C:\laragon\www\gestion-de-eventos-main4      
λ php artisan make:seeder CategoriaSeeder    
Seeder created successfully.                 
                                             
C:\laragon\www\gestion-de-eventos-main4      
λ php artisan make:seeder ClienteSeeder      
Seeder created successfully.                 
                                             
C:\laragon\www\gestion-de-eventos-main4      
λ php artisan make:seeder EmpleadoSeeder     
Seeder created successfully.                 
                                             
C:\laragon\www\gestion-de-eventos-main4      
λ php artisan make:seeder LibreriaSeeder     
Seeder created successfully.                 
                                             
C:\laragon\www\gestion-de-eventos-main4      
λ php artisan make:seeder PedidoSeeder       
Seeder created successfully.                 
                                             
C:\laragon\www\gestion-de-eventos-main4      
λ php artisan make:seeder ProductoSeeder     
Seeder created successfully.                 
                                             
C:\laragon\www\gestion-de-eventos-main4      
λ php artisan make:seeder GeneralSeeder      
Seeder created successfully.                 
                                             
C:\laragon\www\gestion-de-eventos-main4      
λ                                            
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;

## Categoria Seeder
```bash
class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::create(['nombreCategoria' => 'Boligrafos']);
        Categoria::create(['nombreCategoria' => 'Libros']);
        Categoria::create(['nombreCategoria' => 'Cuadernos']);
        Categoria::create(['nombreCategoria' => 'Material de escritorio']);
        Categoria::create(['nombreCategoria' => 'Marcadores']);
        Categoria::create(['nombreCategoria' => 'Cuadernos']);
        Categoria::create(['nombreCategoria' => 'Indumentaria escolar']);
        Categoria::create(['nombreCategoria' => 'Papeleria']);
    }
}
```
## Conclusiones
En conclusión, este proyecto backend desarrollado con Laravel y utilizando HeidiSQL como herramienta de gestión de base de datos representa un paso significativo hacia la creación de una robusta plataforma para esta librería. La combinación de la potencia del framework Laravel y la eficiencia de HeidiSQL nos permite construir una infraestructura escalable, segura y de alto rendimiento.
## Anexos

### foto de la empresa para la cual estan desarrollando
<a data-flickr-embed="true" href="https://www.flickr.com/photos/200265160@N08/53564605832/in/dateposted-public/" title="GISBERT"><img src="https://live.staticflickr.com/65535/53564605832_8a1d382742.jpg" width="500" height="500" alt="GISBERT"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

### ubicacion
[Link de la ubicacion](https://maps.app.goo.gl/Uyt889UzUu8455Gn6)