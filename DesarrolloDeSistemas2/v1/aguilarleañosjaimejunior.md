# proyecto formativo: Modelado Backend de un sistema de Veterinaria IPET.
- Jaime Junior Aguilar Leaños
- [Facebook](https://www.facebook.com/zcjota25)
- [Instagram](https://www.instagram.com/zcjota/)
- [Git](https://www.instagram.com/zcjota/)

##	Introducción
Este sistema backend ha sido diseñado específicamente para transformar y modernizar la gestión de una clínica veterinaria, centralizando los servicios y ventas de productos en una plataforma ágil y moderna. Implementado utilizando Laravel, este proyecto busca ofrecer una solución escalable y eficiente que facilite las operaciones diarias de la Veterinaria Ipet-Rzor.

##	Objetivos
El principal objetivo de este proyecto es crear un backend robusto, escalable y fácil de usar para la Veterinaria Ipet-Rzor, permitiendo una gestión eficiente de servicios, productos, clientes, y mucho más, todo ello a través de Laravel para asegurar una implementación rápida y eficaz.

##	Marco Teórico

###	Laragol
Insatalamos Cualquier  servidor web , en este caso usaremos Laragol
una ves instalado nos iremos asu lineas de comandos para crear el proyecto en laravel.

###	Laravel
Sigue estos pasos para instalar Laravel en tu sistema:

1. **Instalar Composer**: Si aún no tienes Composer instalado, puedes descargarlo e instalarlo desde [getcomposer.org](https://getcomposer.org/).

2. **Crear un Nuevo Proyecto de Laravel**: Utiliza Composer para crear un nuevo proyecto de Laravel ejecutando el siguiente comando en tu terminal:

```bash
composer create-project --prefer-dist laravel/laravel nombre-del-proyecto
```

### Ejecucion de este proyecto 

Para el uso de este proyecto lo bajaremos desde git  

Una vez bajado para ejecutar el proyecto 
verificamos tener composer y php instalados 

composer -v 
php -v

respectivamente para instalar el proyecto usamos 
composer update o composer install 

###	Modelo
una ves terminado seguimos con los pasos de creacion de los modelos

php artisan make:model Cliente
php artisan make:model Menu
php artisan make:model Perfil
php artisan make:model Sucursal
php artisan make:model TipoPago
php artisan make:model CategoriaProducto
php artisan make:model CategoriaServicio
php artisan make:model CategoriaMascota
php artisan make:model SubMenu
php artisan make:model Personal
php artisan make:model DetallePerfil
php artisan make:model TipoServicio
php artisan make:model Producto
php artisan make:model Usuario
php artisan make:model Mascota
php artisan make:model FichaServicio
php artisan make:model DetalleServicio
php artisan make:model Veterinaria
php artisan make:model Tienda
php artisan make:model Vacunacion
php artisan make:model HistorialMedico
php artisan make:model Ingreso


##	Metodología
El desarrollo se basa en el patrón Modelo-Vista-Controlador (MVC), una metodología estructurada que separa la lógica del negocio, la interfaz de usuario y la interacción del usuario en tres componentes distintos. Este enfoque facilita el mantenimiento del código, la prueba de componentes individuales y la colaboración en equipo, al tiempo que permite un desarrollo paralelo de funcionalidades y una integración fluida.

##	Modelado o Sistematización

### Diagrama de clases

![diagrama](https://live.staticflickr.com/65535/53568292813_466bb95d81_h.jpg)
### Diagrama entidad relacion.

### Migracion

Creamos una migracion podemos usar : php artisan make:migration create_nombre_tabla_table

Migrar la base de datos :
php artisan migrate para ejecutar todas las migraciones 
php artisan migrate:refresh para actualizar las migraciones 



        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('app'); 
            $table->string('apm')->nullable(); 
            $table->string('telefono',8)->unique(); 
            $table->string('email')->nullable(); 
            $table->string('ci',15)->unique(); 
            $table->text('referencias')->nullable();
            $table->timestamps();
            $table->softDeletes();
    
        });

        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        
        //TABLA PERFIL
        Schema::create('perfiles', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //TABLA SUCURSAL
        Schema::create('sucursales', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        //TABLA TIPO PAGO
        Schema::create('tipo_pagos', function (Blueprint $table) {
        $table->id();
        $table->string('nombre')->unique();
        $table->timestamps();
        $table->softDeletes();
        });
  
        Schema::create('categorias_productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('categorias_servicios', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('categorias_mascotas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('sub_menus', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->unsignedBigInteger('menu_id');
            $table->string('ruta');
            $table->timestamps();
            $table->softDeletes();
        
            $table->foreign('menu_id')->references('id')->on('menus');
        });

        //TABLA PERSONAL
        Schema::create('personals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('suc_id');
            $table->string('nombre');
            $table->string('app'); 
            $table->string('apm')->nullable(); 
            $table->string('telefono',8)->unique(); 
            $table->text('direccion')->nullable(); 
            $table->string('pin',4)->nullable(); 
            $table->timestamps();
            $table->softDeletes();
        
            $table->foreign('suc_id')->references('id')->on('sucursales');
        });

        Schema::create('detalle_perfiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perfil_id');
            $table->unsignedBigInteger('menu_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('perfil_id')->references('id')->on('perfiles');
            $table->foreign('menu_id')->references('id')->on('menus');
        });

        Schema::create('tipos_servicios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('catg_id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        
            $table->foreign('catg_id')->references('id')->on('categorias_servicios');
        });

        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('categoria_id');
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->decimal('precio', 8, 2);
            $table->integer('stock')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('categoria_id')->references('id')->on('categorias_productos')->onDelete('cascade');
        });
 
         //TABLA USUARIO
         Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perfil_id');
            $table->unsignedBigInteger('personal_id');
            $table->unsignedBigInteger('suc_id');
            $table->string('usuario');
            $table->string('password');
            $table->string('email')->nullable(); 
            $table->timestamps();
            $table->softDeletes();
        
            $table->foreign('perfil_id')->references('id')->on('perfiles');
            $table->foreign('personal_id')->references('id')->on('personals');
            $table->foreign('suc_id')->references('id')->on('sucursales');
        });

       

        Schema::create('mascotas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cliente_id');
            $table->string('nombre');
            $table->string('raza')->default('mestizo');
            $table->string('sexo',1);
            $table->string('color')->nullable(); 
            $table->unsignedBigInteger('catgm_id');
            $table->text('cuidado_especial')->nullable(); 
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('catgm_id')->references('id')->on('categorias_mascotas');
            $table->foreign('cliente_id')->references('id')->on('clientes');
        });
        

        Schema::create('ficha_servicios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tipo_serv');
            $table->unsignedBigInteger('mast_id');
            $table->unsignedBigInteger('cliente_id');
            $table->date('fecha');
            $table->dateTime('fecha_serv')->nullable();
            $table->unsignedBigInteger('personal_id');
            $table->unsignedBigInteger('producto_id')->nullable(); 
            $table->string('recomendaciones')->nullable(); 
            $table->string('estado_serv')->default('INI');
            $table->string('m_descripcion')->nullable(); 
            $table->string('personal_fin')->nullable(); 
            $table->text('memo_anu')->nullable(); 
            $table->string('personal_anul')->nullable(); 
            $table->unsignedBigInteger('suc_id');
            $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('tipo_serv')->references('id')->on('tipos_servicios');
            $table->foreign('mast_id')->references('id')->on('mascotas');
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->foreign('personal_id')->references('id')->on('personals');
            $table->foreign('producto_id')->references('id')->on('productos');
            $table->foreign('suc_id')->references('id')->on('sucursales');
        });

        Schema::create('detalle_servicios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fs_id');
            $table->unsignedBigInteger('tipo_serv');
            $table->string('descripcion');
            $table->string('sub_total');
            $table->string('cantidad')->nullable();
            $table->string('monto');
            $table->string('pagado');
            $table->string('estado_pago')->nullable(); 
            $table->dateTime('fecha_registro');
            $table->unsignedBigInteger('personal_id');
            $table->text('comentarios')->nullable(); 
            $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('fs_id')->references('id')->on('ficha_servicios');
            $table->foreign('personal_id')->references('id')->on('personals');
           
        });
      
        Schema::create('veterinarias', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fs_id');
            $table->string('nr');
            $table->string('nr_global');
            $table->string('sub_total');
            $table->decimal('total', 8, 2);
            $table->decimal('total_pagado', 8, 2);
            $table->string('nombre');
            $table->string('telefono')->nullable(); 
            $table->string('direccion')->nullable(); 
            $table->string('mascota');
            $table->string('sexo',1)->nullable(); 
            $table->dateTime('fecha');
            $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('fs_id')->references('id')->on('ficha_servicios');
        });

        Schema::create('tiendas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fs_id');
            $table->string('nr');
            $table->string('nr_global');
            $table->string('sub_total');
            $table->decimal('total', 8, 2);
            $table->decimal('total_pagado', 8, 2);
            $table->string('nombre');
            $table->string('telefono')->nullable(); 
            $table->string('direccion')->nullable(); 
            $table->string('mascota'); 
            $table->string('sexo')->nullable(); 
            $table->dateTime('fecha');
            $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('fs_id')->references('id')->on('ficha_servicios');
        });

        Schema::create('vacunaciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mascota_id');
            $table->string('nombre_vacuna');
            $table->date('fecha_vacunacion');
            $table->string('proxima_vacunacion')->nullable();
            $table->unsignedBigInteger('ficha_servicio_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        
            $table->foreign('mascota_id')->references('id')->on('mascotas');
            $table->foreign('ficha_servicio_id')->references('id')->on('ficha_servicios');
        });

        Schema::create('historial_medicos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mascota_id');
            $table->date('fecha');
            $table->text('descripcion');
            $table->string('tratamiento')->nullable();
            $table->unsignedBigInteger('ficha_servicio_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        
            $table->foreign('ficha_servicio_id')->references('id')->on('ficha_servicios');
            $table->foreign('mascota_id')->references('id')->on('mascotas');
        });

        Schema::create('ingresos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fs_id');
            $table->unsignedBigInteger('tipo_serv');
            $table->unsignedBigInteger('prod_id');
            $table->unsignedBigInteger('personal_id');
            $table->string('nr');
            $table->dateTime('fecha');
            $table->string('mascota'); 
            $table->string('cantidad');
            $table->decimal('total', 8, 2);
            $table->decimal('total_pagado', 8, 2);
            $table->unsignedBigInteger('tipo_pago');
            $table->string('m_descripcion')->nullable(); 
            $table->string('personal_fin')->nullable(); 
            $table->text('memo_anu')->nullable(); 
            $table->string('personal_anul')->nullable();      
            $table->unsignedBigInteger('suc_id');
            $table->timestamps();
            $table->softDeletes();

            
            $table->foreign('tipo_serv')->references('id')->on('tipos_servicios');
            $table->foreign('fs_id')->references('id')->on('ficha_servicios');
            $table->foreign('personal_id')->references('id')->on('personals');
            $table->foreign('suc_id')->references('id')->on('sucursales');
            $table->foreign('tipo_pago')->references('id')->on('tipo_pagos');
        });


### explicacion del un caso de modelado.
El caso de modelado, ilustra la relación entre los servicios ofrecidos por la veterinaria y las mascotas registradas. Cada servicio está vinculado a un tipo específico y puede incluir detalles como la mascota atendida, el personal asignado, y la fecha del servicio. Este modelo proporciona una base para el seguimiento detallado de los servicios, esencial para la gestión eficiente de la veterinaria, esto se mejorara conforme el proyecto avance.

### Datos seeder

Para crear un seeder podemos usar 
php artisan make:seeder NombreTablaSeeder

Ponemos los modelos de las tablas 

Una vez puestos ponemos el comando 

php artisan db:seed
para ejuctar el seeder configurado

use App\Models\Perfil;
use App\Models\Sucursal;
use App\Models\TipoPago;
use App\Models\Menu;
use App\Models\SubMenu;
use App\Models\CategoriaProducto;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\Personal;
use App\Models\DetallePerfil;
use App\Models\Usuario;
use App\Models\CategoriaServicio;
use App\Models\CategoriaMascota;
use App\Models\TipoServicio;
use App\Models\Mascota;
use App\Models\FichaServicio;
use App\Models\DetalleServicio;
use App\Models\Veterinaria;
use App\Models\Tienda;
use App\Models\Ingreso;
use App\Models\Vacunacion;
use App\Models\HistorialMedico;

use Faker\Factory as Faker;


creamoes los seeder en el orden correcto esto para no afectar en las llaves foraneas 
//CLIENTES
        $faker = Faker::create('es_ES'); // Configura Faker en español

        for ($i = 0; $i < 50; $i++) { // Genera 50 clientes de ejemplo
            Cliente::create([
                'nombre' => $faker->firstName,
                'app' => $faker->lastName,
                'apm' => $faker->lastName,
                'telefono' => $faker->unique()->numerify('########'), // Genera un número de 8 dígitos
                'email' => $faker->unique()->safeEmail,
                'ci' => $faker->unique()->numerify('#########'),
                'referencias' => $faker->sentence
            ]);
        }


        //PERFILES
        $perfiles = [
            ['nombre' => 'AdmSytem', 'descripcion' => 'Acceso total al sistema'],
            ['nombre' => 'Administrador', 'descripcion' => 'Acceso al sistema '],
            ['nombre' => 'Cajero', 'descripcion' => 'Gestión de caja y facturación'],
            ['nombre' => 'Auxiliar', 'descripcion' => 'Soporte en diversas actividades'],
            ['nombre' => 'Veterinario', 'descripcion' => 'Encargado de la atención médica'],
            ['nombre' => 'Recepcionista', 'descripcion' => 'Gestión de citas y atención al cliente'],
        ];

        foreach ($perfiles as $perfil) {
            Perfil::create($perfil);
        }

        //SUCURSALES
        $sucursales = [
            ['nombre' => 'Veterinaria Equipetrol'],
            ['nombre' => 'Veterinaria Norte'],
            ['nombre' => 'Veterinaria Sur'],
        ];

        foreach ($sucursales as $sucursal) {
            Sucursal::create($sucursal);
        }

        //PERSONAL
        $sucursales = Sucursal::all()->pluck('id')->toArray(); // Obtiene todos los IDs de sucursales

        foreach ($sucursales as $sucId) {
            for ($i = 0; $i < 10; $i++) { // Supongamos que queremos 10 empleados por sucursal
                Personal::create([
                    'suc_id' => $sucId,
                    'nombre' => $faker->firstName,
                    'app' => $faker->lastName,
                    'apm' => $faker->lastName,
                    'telefono' => $faker->unique()->numerify('########'), // Genera un número de teléfono único de 8 dígitos
                    'direccion' => $faker->address,
                    'pin' => $faker->numerify('####') // Genera un PIN de 4 dígitos
                ]);
            }
        }

        //TIPOS DE PAGO
        $tiposPago = [
            ['nombre' => 'Efectivo'],
            ['nombre' => 'Tarjeta de Crédito'],
            ['nombre' => 'Transferencia Bancaria'],
            ['nombre' => 'Pago Móvil'],
        ];
        
        foreach ($tiposPago as $tipoPago) {
            TipoPago::create($tipoPago);
        }

        //CATEGORIA SERVICIOS
        $categoriasServicio = [
            ['nombre' => 'Consulta General'],
            ['nombre' => 'Vacunación'],
            ['nombre' => 'Cirugía'],
            ['nombre' => 'Estética Canina'],
        ];
        
        foreach ($categoriasServicio as $categoriaServicio) {
            CategoriaServicio::create($categoriaServicio);
        }

        //TIPOS DE SERVICIOS
        $tiposServicio = [
            // Asumiendo que ya has creado las categorías de servicio y tienes sus IDs.
            ['catg_id' => 1, 'nombre' => 'Consulta Veterinaria'],
            ['catg_id' => 2, 'nombre' => 'Vacuna Rabia'],
            ['catg_id' => 3, 'nombre' => 'Esterilización'],
            ['catg_id' => 4, 'nombre' => 'Baño y Corte de Pelo'],
        ];
        
        foreach ($tiposServicio as $tipoServicio) {
            TipoServicio::create($tipoServicio);
        }

        //CATEGORIAS MASCOTAS
        $categoriasMascota = [
            ['nombre' => 'Perros'],
            ['nombre' => 'Gatos'],
        ];
        
        foreach ($categoriasMascota as $categoriaMascota) {
            CategoriaMascota::create($categoriaMascota);
        }

        // CATEGORIAS DE PRODUCTOS
        $categoriasProductos = [
        ['nombre' => 'Alimentos', 'descripcion' => 'Alimentos balanceados para mascotas'],
        ['nombre' => 'Medicamentos', 'descripcion' => 'Medicamentos y suplementos para la salud de las mascotas'],
        ['nombre' => 'Accesorios', 'descripcion' => 'Accesorios diversos para mascotas'],
        ['nombre' => 'Juguetes', 'descripcion' => 'Juguetes para entretenimiento de mascotas'],
        ['nombre' => 'Higiene', 'descripcion' => 'Productos de higiene y cuidado para mascotas'],
        ];

         foreach ($categoriasProductos as $categoriaProducto) {
         CategoriaProducto::create($categoriaProducto);
         }

         // MENUS
        $menus = [
        ['nombre' => 'Inicio'],
        ['nombre' => 'Gestión de Clientes'],
        ['nombre' => 'Gestión de Pacientes (Mascotas)'],
        ['nombre' => 'Servicios Veterinarios'],
        ['nombre' => 'Gestión de Productos'],
        ['nombre' => 'Gestión de Ventas'],
        ['nombre' => 'Reportes'],
        ['nombre' => 'Configuraciones'],
        ];

         foreach ($menus as $menu) {
         Menu::create($menu);
         }
         // Primero, encontrar los IDs de los menús para asociarlos correctamente
        $gestionClientesMenuId = Menu::where('nombre', 'Gestión de Clientes')->first()->id;
        $serviciosVeterinariosMenuId = Menu::where('nombre', 'Servicios Veterinarios')->first()->id;
        $gestionPacientesMenuId = Menu::where('nombre', 'Gestión de Pacientes (Mascotas)')->first()->id;

        // Sub Menús para 'Gestión de Clientes'
        SubMenu::create([
            'nombre' => 'Registrar Cliente',
            'menu_id' => $gestionClientesMenuId,
            'ruta' => 'clientes/registrar'
        ]);
        SubMenu::create([
            'nombre' => 'Listar Clientes',
            'menu_id' => $gestionClientesMenuId,
            'ruta' => 'clientes/listar'
        ]);

        // Sub Menús para 'Servicios Veterinarios'
        SubMenu::create([
            'nombre' => 'Agregar Servicio',
            'menu_id' => $serviciosVeterinariosMenuId,
            'ruta' => 'servicios/agregar'
        ]);
        SubMenu::create([
            'nombre' => 'Consultar Servicios',
            'menu_id' => $serviciosVeterinariosMenuId,
            'ruta' => 'servicios/consultar'
        ]);

        // Sub Menús para 'Gestión de Pacientes (Mascotas)'
        SubMenu::create([
            'nombre' => 'Registrar Mascota',
            'menu_id' => $gestionPacientesMenuId,
            'ruta' => 'pacientes/registrar'
        ]);
        SubMenu::create([
            'nombre' => 'Historial de Mascotas',
            'menu_id' => $gestionPacientesMenuId,
            'ruta' => 'pacientes/historial'
        ]);

        

        //DETALLE PERFIL
        $accesos = [
            'AdmSytem' => ['Inicio', 'Gestión de Clientes', 'Gestión de Pacientes (Mascotas)', 'Servicios Veterinarios', 'Gestión de Productos', 'Gestión de Ventas', 'Reportes', 'Configuraciones'],
            'Administrador' => ['Inicio', 'Gestión de Clientes', 'Gestión de Pacientes (Mascotas)', 'Servicios Veterinarios', 'Gestión de Productos', 'Gestión de Ventas', 'Reportes'],
            'Cajero' => ['Inicio', 'Gestión de Ventas', 'Reportes'],
            'Auxiliar' => ['Inicio', 'Gestión de Clientes', 'Gestión de Pacientes (Mascotas)'],
            'Veterinario' => ['Inicio', 'Gestión de Pacientes (Mascotas)', 'Servicios Veterinarios', 'Reportes'],
            'Recepcionista' => ['Inicio', 'Gestión de Clientes', 'Gestión de Pacientes (Mascotas)', 'Servicios Veterinarios'],
        ];

        foreach ($accesos as $perfilNombre => $menusAcceso) {
            $perfilId = Perfil::where('nombre', $perfilNombre)->first()->id;
            foreach ($menusAcceso as $menuNombre) {
                $menuId = Menu::where('nombre', $menuNombre)->first()->id;
                DetallePerfil::create([
                    'perfil_id' => $perfilId,
                    'menu_id' => $menuId,
                ]);
            }
        }

       //PRODUCTOS
        $categorias = CategoriaProducto::pluck('id')->toArray(); // Obtiene todos los IDs de las categorías de productos

        // Productos específicos de veterinaria
        $nombresProductos = [
            'Alimento para perros', 'Alimento para gatos', 'Vacunas para perros',
            'Vacunas para gatos', 'Antipulgas', 'Collares antiparasitarios',
            'Shampoo medicado', 'Suplementos vitamínicos', 'Juguetes para perros',
            'Juguetes para gatos', 'Camas para mascotas', 'Ropa para mascotas'
        ];

        foreach ($nombresProductos as $nombre) {
            Producto::create([
                'categoria_id' => $faker->randomElement($categorias), // Asigna una categoría aleatoria
                'nombre' => $nombre,
                'descripcion' => $faker->sentence(6, true), // Genera una descripción aleatoria
                'precio' => $faker->randomFloat(2, 10, 200), // Genera un precio aleatorio entre 10 y 200
                'stock' => $faker->numberBetween(10, 100) // Genera un stock aleatorio entre 10 y 100
            ]);
        }

        //USUARIOS 
        $perfiles = Perfil::pluck('id')->toArray();
        $personals = Personal::pluck('id')->toArray();
        $sucursales = Sucursal::pluck('id')->toArray();

        for ($i = 0; $i < 20; $i++) { // Genera 20 usuarios de ejemplo
            $email = $faker->unique()->safeEmail;

            Usuario::create([
                'perfil_id' => $faker->randomElement($perfiles),
                'personal_id' => $faker->randomElement($personals),
                'suc_id' => $faker->randomElement($sucursales),
                'usuario' => explode('@', $email)[0], // Usa la parte local del correo electrónico como nombre de usuario
                'password' => Hash::make('password'), // Usa una contraseña predeterminada, en un caso real debería ser más segura
                'email' => $email
            ]);
        }

        //MASCOTAS
        $clientes = Cliente::pluck('id')->toArray();
        $categoriasMascotas = CategoriaMascota::pluck('id')->toArray();

        for ($i = 0; $i < 100; $i++) { // Genera 100 mascotas de ejemplo
            Mascota::create([
                'cliente_id' => $faker->randomElement($clientes),
                'nombre' => $faker->firstName,
                'raza' => $faker->randomElement(['mestizo', 'labrador', 'pastor alemán', 'bulldog', 'beagle', 'poodle', 'schnauzer']),
                'sexo' => $faker->randomElement(['M', 'F']),
                'color' => $faker->safeColorName,
                'catgm_id' => $faker->randomElement($categoriasMascotas),
                'cuidado_especial' => $faker->boolean(30) ? $faker->sentence : null // 30% de probabilidad de que tenga cuidados especiales
            ]);
        }

       //FICHA SERVICIOS
$tipoServicios = TipoServicio::pluck('id')->toArray();
$mascotas = Mascota::pluck('id')->toArray();
$clientes = Cliente::pluck('id')->toArray();
$personals = Personal::pluck('id')->toArray();
$sucursales = Sucursal::pluck('id')->toArray();
$productos = Producto::pluck('id')->toArray(); // Opcional, si se asignará un producto a cada servicio
        
for ($i = 0; $i < 100; $i++) {
    $mascotaId = $faker->randomElement($mascotas);
    $mascota = Mascota::find($mascotaId);
        
    $fichaServicio = FichaServicio::create([
        'tipo_serv' => $faker->randomElement($tipoServicios),
        'mast_id' => $mascotaId,
        'cliente_id' => $mascota->cliente_id, // Asegura que el cliente sea el propietario de la mascota
        'fecha' => $faker->date(),
        'fecha_serv' => $faker->dateTimeBetween('-1 month', '+1 month'),
        'personal_id' => $faker->randomElement($personals),
        'producto_id' => $faker->randomElement($productos), // Opcional
        'recomendaciones' => $faker->sentence,
        'estado_serv' => 'INI',
        'm_descripcion' => $faker->sentence,
        'suc_id' => $faker->randomElement($sucursales),
    ]);
        
    // Crear detalles de servicios directamente aquí
    for ($j = 0; $j < $faker->numberBetween(1, 5); $j++) {
        DetalleServicio::create([
            'fs_id' => $fichaServicio->id,
            'tipo_serv' => $faker->randomElement($tipoServicios),
            'descripcion' => $faker->sentence,
            'sub_total' => $faker->randomFloat(2, 10, 100),
            'cantidad' => $faker->numberBetween(1, 10),
            'monto' => $faker->randomFloat(2, 50, 500),
            'pagado' => $faker->randomElement(['Sí', 'No']),
            'estado_pago' => $faker->randomElement(['Pagado', 'Pendiente']),
            'fecha_registro' => $faker->dateTimeThisYear(),
            'personal_id' => $fichaServicio->personal_id,
            'comentarios' => $faker->sentence,
        ]);
    }
}

 $fichaServicios = FichaServicio::all();

        foreach ($fichaServicios as $fs) {
            $mascota = Mascota::find($fs->mast_id);
            $cliente = Cliente::find($fs->cliente_id);
        
            Veterinaria::create([
                'fs_id' => $fs->id,
                'nr' => $faker->unique()->randomNumber(),
                'nr_global' => $faker->unique()->randomNumber(),
                'sub_total' => $faker->randomFloat(2, 100, 200),
                'total' => $faker->randomFloat(2, 200, 300),
                'total_pagado' => $faker->randomFloat(2, 100, 200),
                'nombre' => $cliente->nombre,
                'telefono' => $cliente->telefono,
                'direccion' => $cliente->direccion,
                'mascota' => $mascota->nombre,
                'sexo' => $mascota->sexo,
                'fecha' => $fs->fecha_serv,
            ]);
        
            Tienda::create([
                'fs_id' => $fs->id,
                'nr' => $faker->unique()->randomNumber(),
                'nr_global' => $faker->unique()->randomNumber(),
                'sub_total' => $faker->randomFloat(2, 100, 200),
                'total' => $faker->randomFloat(2, 200, 300),
                'total_pagado' => $faker->randomFloat(2, 100, 200),
                'nombre' => $cliente->nombre,
                'telefono' => $cliente->telefono,
                'direccion' => $cliente->direccion,
                'mascota' => $mascota->nombre,
                'sexo' => $mascota->sexo,
                'fecha' => $fs->fecha_serv,
                
            ]);
        }

        $mascotas = Mascota::all();

foreach ($mascotas as $mascota) {
    Vacunacion::create([
        'mascota_id' => $mascota->id,
        'nombre_vacuna' => $faker->word,
        'fecha_vacunacion' => $faker->date(),
        'proxima_vacunacion' => $faker->date(),
        'ficha_servicio_id' => $faker->randomElement(FichaServicio::pluck('id')->toArray()),
    ]);
}


foreach ($mascotas as $mascota) {
    HistorialMedico::create([
        'mascota_id' => $mascota->id,
        'fecha' => $faker->date(),
        'descripcion' => $faker->paragraph,
        'tratamiento' => $faker->sentence,
        'ficha_servicio_id' => $faker->randomElement(FichaServicio::pluck('id')->toArray()),
    ]);
}

$tipoServicios = TipoServicio::pluck('id')->toArray();
$personals = Personal::pluck('id')->toArray();
$sucursales = Sucursal::pluck('id')->toArray();
$tipoPagos = TipoPago::pluck('id')->toArray();
$productos = Producto::pluck('id')->toArray();

foreach ($fichaServicios as $fs) {
    Ingreso::create([
        'fs_id' => $fs->id,
        'tipo_serv' => $faker->randomElement($tipoServicios),
        'prod_id' => $faker->randomElement($productos),
        'personal_id' => $faker->randomElement($personals),
        'nr' => $faker->randomNumber(),
        'fecha' => $fs->fecha_serv,
        'mascota' => Mascota::find($fs->mast_id)->nombre,
        'cantidad' => $faker->numberBetween(1, 10),
        'total' => $faker->randomFloat(2, 100, 500),
        'total_pagado' => $faker->randomFloat(2, 100, 500),
        'tipo_pago' => $faker->randomElement($tipoPagos),
        'suc_id' => $faker->randomElement($sucursales),
    ]);
}


### APi.

No olvides de poner todos los modelos para usar las api correspondientes 

use App\Http\Controllers\Api\UserController;
use App\Models\Cliente;
use App\Models\Menu;
use App\Models\Perfil;
use App\Models\Sucursal;
use App\Models\TipoPago;
use App\Models\CategoriaProducto;
use App\Models\CategoriaServicio;
use App\Models\CategoriaMascota;
use App\Models\SubMenu;
use App\Models\Personal;
use App\Models\DetallePerfil;
use App\Models\Usuario;
use App\Models\Mascota;
use App\Models\FichaServicio;
use App\Models\DetalleServicio;
use App\Models\Veterinaria;
use App\Models\Tienda;
use App\Models\Vacunacion;
use App\Models\HistorialMedico;
use App\Models\Ingreso;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------

|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('clientes', function () {
    return Cliente::all();
});


Route::get('menus', function () {
    return Menu::all();
});

Route::get('perfiles', function () {
    return Perfil::all();
});

Route::get('sucursales', function () {
    return Sucursal::all();
});

Route::get('tipo_pagos', function () {
    return TipoPago::all();
});

Route::get('categorias_productos', function () {
    return CategoriaProducto::all();
});

Route::get('categorias_servicios', function () {
    return CategoriaServicio::all();
});

Route::get('categorias_mascotas', function () {
    return CategoriaMascota::all();
});

Route::get('sub_menus', function () {
    return SubMenu::all();
});

Route::get('personals', function () {
    return Personal::all();
});

Route::get('detalle_perfiles', function () {
    return DetallePerfil::all();
});

Route::get('usuarios', function () {
    return Usuario::all();
});

Route::get('mascotas', function () {
    return Mascota::all();
});

Route::get('ficha_servicios', function () {
    return FichaServicio::all();
});

Route::get('detalle_servicios', function () {
    return DetalleServicio::all();
});

Route::get('veterinarias', function () {
    return Veterinaria::all();
});

Route::get('tiendas', function () {
    return Tienda::all();
});

Route::get('vacunaciones', function () {
    return Vacunacion::all();
});

Route::get('historial_medicos', function () {
    return HistorialMedico::all();
});

Route::get('ingresos', function () {
    return Ingreso::all();
});


Route::get('clientesConMascotas', function () {
    $clientesConMascotas = Cliente::with('mascotas')->get();
    return response()->json($clientesConMascotas);
});

Route::get('clientes_ig', function () {
    $clientesConIngresosFinalizados = Cliente::whereHas('mascotas.fichaServicios', function ($query) {
        $query->whereHas('ingresos', function ($query) {
            $query->where('estado_serv', 'FIN'); // Asumiendo que 'FIN' es el estado finalizado
        });
    })->with(['mascotas.ficha_servicios.ingresos'])->get();

    return response()->json($clientesConIngresosFinalizados);
});



##	Conclusiones

El proyecto para la veterinaria IPET, desarrollado en Laravel, transforma significativamente la gestión de servicios y ventas, mejorando la eficiencia y la experiencia del 
cliente. Con un enfoque en la escalabilidad y la facilidad de mantenimiento, establece una base sólida para el crecimiento futuro y la innovación en el sector veterinario.


##	Bibliografía
-  Repositorio de ejemplo usado 
https://gitlab.com/tecnoprofe/gestion-de-eventos

##	Anexos

Primeros pasos de modelado 

![diagrama](https://live.staticflickr.com/65535/53558757243_5308db8960_b.jpg)


### foto de la empresa para la cual estan desarrollando

![diagrama](https://live.staticflickr.com/65535/53568304913_0c204ab662_b.jpg)

### ubicacion

https://maps.app.goo.gl/qjMc1bNNvnVadk3p7